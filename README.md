# Progetto “Introduzione alla programmazione per il web”
###
Membri del gruppo:
- Alessio Gottardi 192295
- Diego Cappelletti 185644
- Radu Loghin 192313

Il progetto è stato sviluppato con le tecnologie JSP/SERVLET/JSTL/HTML viste a lezione e seguendo i pattern MVC e DAO.
Per il DAO abbiamo usato le due librerie **persistence-jdbc-impl** e **persistence-api** viste durante il laboratorio (sono state aggiunte nella cartella dipendenze_aggiuntive)

## Documentazione

#### Pagine html/jsp:
- **home.html:** pagina di benvenuto dalla quale si può andare alla login o a leggere le informazioni sulla privacy.
- **login.jsp:** pagina in cui l’utente inserisce le proprie credenziali e a secondo del tipo di utente (medico/paziente/ssp), viene indirizzato alla sua “pagina profilo.
- **pazienteProfilo.jsp:** pagina profilo del paziente in cui può visualizzare tutti i suoi dati, accedere alle altre sezioni, scaricare i propri ticket o fare il logout.
- **pazienteRicette.jsp:** pagina in cui il paziente visualizza tutte le sue ricette e per ognuna la può scaricare in formato pdf con il qr code della ricetta.
- **pazienteEsami.jsp:** pagina in cui il paziente visualizza tutti i suoi esami.
- **pazienteVisite.jsp:** pagina in cui il paziente visualizza tutte le sue visite.
- **medicoProfilo.jsp:** pagina profilo del medico in cui visualizza tutti i suoi dati e da cui può accedere alla lista dei suoi pazienti.
- **medicoPazienti.jsp:** pagina in cui il medico vede la lista dei propri pazienti o in caso del medico specialista, la lista di tutti i pazienti nel sistema.
- **medicoBSchedaPaziente.jsp:** pagina per il medico di base: vede tutte le info del paziente e può prescrivere nuovi esami/farmaci/visite.
- **medicoSSchedaPaziente.jsp:** pagina per il medico specialista: vede tutte le info del paziente e può eseguire le visite della sua specializzazione.
- **sspProfilo.jsp:** pagina profilo per gli ssp. Qua si può generare il report delle ricette o accedere alla lista dei pazienti.
- **sspPazienti.jsp:** pagina in cui l'ssp vede la lista dei propri pazienti o in caso del medico specialista, la lista di tutti i pazienti nel sistema.
- **sspSchedaPaziente.jsp:** qui l'ssp può eseguire gli esami del paziente

#### Servlet:
- **LoginServlet:** verifica le credenziali inserite. Se l’utente esiste, viene reindirizzato alla sua pagina profilo. Crea i cookie se l’utente ha messo la spunta su "ricordami".
- **LogoutServlet:** cancella la sessione corrente e i cookie se esistono.
- **PDFServlet:** prende l’id dell’utente della sessione e recupera tutti gli esami/visite/ricette fatte e le stampa su un file pdf. La libreria usata per generare i pdf è boxable (la stessa usata durante le esercitazioni).
- **QRServlet:** stampa una ricetta su un file pdf aggiungendoci anche un codice qr con le informazioni essenziali. La libreria usata per la generazione dei qr code è zxhing (la stessa vista a lezione).
- **ReportServlet:** usata dagli ssp per stampare tutte le ricette erogate in una certa provincia. Libreria usata: apache poi in quanto, a differenza di OpenXls, offriva delle funzioni di styling aggiuntive.
- **PazienteServlet:** usata dai pazienti per cambiare medico di base.
- **MBaseServlet:** usata dal medico di base per prescrivere nuovi esami/visite/ricette.
- **SpecialistaServlet:** usata dai medici specialisti per eseguire le visite.
- **SspServlet:** usata dagli ssp per eseguire gli esami.
- **FotoServlet:** usata dai pazienti per cambiare la propria foto profilo.
- **CambiaPswServlet:** usata da tutti gli utenti per cambiare la password.

#### Filtri:
- **AuthenticationFilter:** controlla che l'utente sia autenticato.
- **PazienteProfiloFilter:** aggiunge i dati del paziente.
- **PazienteEsamiFilter:** aggiunge gli esami del paziente.
- **PazienteVisiteFilter:** aggiunge le visite del paziente.
- **PazienteRicetteFilter:** aggiunge le ricette del paziente.
- **MedicoProfiloFilter:** aggiunge i dati del medico.
- **MedicoPazientiFilter:**  aggiunge i pazienti del medico.
- **MedicoBSchedaPazienteFilter:** aggiunge le informazioni del paziente per il medico di base.
- **MedicoSSchedaPazienteFilter:** aggiunge le informazioni del paziente per il medico di specialista.
- **SspProfiloFilter:** aggiunge i dati del'ssp.
- **SspPazientiFilter:** aggiunge i pazienti dell'ssp.
- **SspSchedaPazienteFilter:**  aggiunge le informazioni del paziente per l'ssp.